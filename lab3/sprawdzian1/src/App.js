import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Home from './ui/home/Home';
import PaczkiList from './ui/paczki/paczkiList';
import PaczkiAdd from './ui/paczki/paczkiAdd';
import PaczkiEdit from './ui/paczki/paczkiEdit';
import PaczkiDetails from './ui/paczki/paczkiDetails';

function App() {
  return (
    <Router>
      <div className="Main">
        <nav>
          <ul>
            <li>
              <Link to="/" className="bntLink">Home</Link>
            </li>
            <li>
              <Link to="/paczki" className="bntLink">Paczki</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/paczki/details/:id"><PaczkiDetails /></Route>
          <Route path="/paczki/edit/:id"><PaczkiEdit /></Route>
          <Route path="/paczki/add"><PaczkiAdd /></Route>
          <Route path="/paczki"><PaczkiList /></Route>

          <Route path="/"><Home /></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
