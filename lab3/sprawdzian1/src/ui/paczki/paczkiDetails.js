import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const PaczkiDetails = () =>{
    let x = window.location.pathname.slice(16)

    const paczki = useSelector((state) => state.paczki)
    const paczka = paczki.find(paczka => paczka.id === x)

    return(
        <div>
            <h1>Detale Paczki</h1>

            <div className="box">
                <ul className="detailsList">
                    <li>Id: {paczka.id}</li>
                    <li>Imie odbiorcy: {paczka.name}</li>
                    <li>Nazwisko odbiorcy: {paczka.lastname}</li>
                    <li>Cena NFT: {paczka.price}</li>
                    <li>Kolor: {paczka.color}</li>
                </ul>
            </div>
            <Link to="/paczki" className="bntLink">Back</Link>
        </div>
        
    )
}

export default PaczkiDetails