import { useSelector } from "react-redux"; 
import { Link } from "react-router-dom";

const PaczkiList = () => {
    const paczki = useSelector((state) => state.paczki);

    return(
        <div>
            <h1>Lista paczek</h1>
            <Link to={"paczki/add"} className="bntLink">Dodaj paczkę</Link>
            <div className="Lista">
                {paczki ? paczki.map(paczka => (
                    <div key={paczka.id} className="ListContainer">
                        <h3>{paczka.title}</h3>
                        <Link to={"paczki/edit/" + paczka.id} className="bntLink">Edit</Link>
                        <Link to={"paczki/details/" + paczka.id} className="bntLink">Details</Link>
                    </div>
                )): <>Empty</>}

            </div>
        </div>
    )
}

export default PaczkiList