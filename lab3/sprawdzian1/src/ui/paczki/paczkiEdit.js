import {useDispatch, useSelector } from "react-redux";
import { ErrorMessage, Field, Form, Formik } from "formik"
import { withRouter } from "react-router";
import { editPaczkaAction } from "../../ducks/actions/paczkiAction";
import * as Yup from 'yup';

const PaczkiEdit = ({history}) => {
    let x = window.location.pathname.slice(13)

    const paczki = useSelector((state) => state.paczki)
    const paczka = paczki.find(paczka => paczka.id === x)

    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(editPaczkaAction(values));
        history.push('/paczki')
    }

    const filmSchema = Yup.object().shape({
        title: Yup.string().required(),
        name: Yup.string().required(),
        lastname: Yup.string().required(),
    })
    
    return(
        <div>
            <h1>Edycja Paczki</h1>
            <Formik
                initialValues={{
                    id: paczka.id,
                    title: paczka.title,
                    name: paczka.name,
                    lastname: paczka.lastname,
                    price: paczka.price,
                    color: paczka.color
                }}
                onSubmit={(values) => handleSubmit(values)}
                enableReinitialize={true}
                validationSchema={filmSchema}>
                
                <Form>
                    <label htmlFor="title">title: </label><br/>
                    <Field name="title" id="title"/><br/>
                    <ErrorMessage name="title" component="div"/>

                    <label htmlFor="name">name: </label><br/>
                    <Field name="name" id="name"/><br/>
                    <ErrorMessage name="name" component="div"/>

                    <label htmlFor="lastname">lastname: </label><br/>
                    <Field name="lastname" id="lastname"/><br/>
                    <ErrorMessage name="lastname" component="div"/>

                    <div id="my-radio-group">Picked</div>
                        <div role="group" aria-labelledby="my-radio-group">
                            <label>
                            <Field type="radio" name="color" value="Zielony" />
                            Zielony
                            </label>
                            <label>
                            <Field type="radio" name="color" value="Czerowny" />
                            Czerwony
                            </label>
                            <label>
                            <Field type="radio" name="color" value="Niebieski" />
                            Niebieski
                            </label>
                            <label>
                            <Field type="radio" name="color" value="Czarny" />
                            Czarny
                            </label>
                            
                        </div>

                    <button type="submit">Submit</button>
                </Form>
            </Formik>
        </div>
    )
}

export default withRouter(PaczkiEdit)