export const ADD_PACZKA = 'ADD_PACZKA'
export const DELETE_PACZKA = 'DELETE_PACZKA'
export const EDIT_PACZKA = 'EDIT_PACZKA'

export const addPaczkaAction = (payload) => ({
    type: 'ADD_PACZKA',
    payload
})

export const deletePaczkaAction = (payload) => ({
    type: 'DELETE_PACZKA',
    payload
})

export const editPaczkaAction = (payload) => ({
    type: 'EDIT_PACZKA',
    payload
})