import { ADD_PACZKA, EDIT_PACZKA, DELETE_PACZKA } from "../actions/paczkiAction";
import { v4 as uuidv4 } from 'uuid'

export const paczkiReducer = (state = [], action) => {
    const payload = action.payload

    switch(action.type) {
        case ADD_PACZKA:
            return [...state, {
                id: uuidv4(),
                title: payload.title,
                name: payload.name,
                lastname: payload.lastname,
                price: payload.price,
                color: payload.color
            }];

        case DELETE_PACZKA:
            return state.filter(el => el.id !== payload.id);

        case EDIT_PACZKA:
            return state.map(el => {
                if (el.id === payload.id){
                    el.title = payload.title;
                    el.name = payload.name;
                    el.lastname = payload.lastname;
                    el.price = payload.price;
                    el.color = payload.color
                }
                return el
            })
        default: 
            return state;
    }
}